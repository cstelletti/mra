package tdd.training.mra;

import java.util.List;

public class Rover {
	String dir;
	int x;
	int y;
	public Rover(int x1, int y1, String dir1) {
		this.x=x1;
		this.y=y1;
		this.dir=dir1;
	}
	public String getDir() {
		return dir;
	}
	public void setDir(String dir1) {
		this.dir = dir1;
	}
	public int getX() {
		return x;
	}
	public void setX(int x1) {
		this.x = x1;
	}
	public int getY() {
		return y;
	}
	public void setY(int y1) {
		this.y = y1;
	}
	
	public String getStatus(List<String> obstacles) {
		StringBuilder strbuilder = new StringBuilder();
		strbuilder.append("("+x+","+y+","+dir+")");
		if(obstacles!=null) {
			for(String ob : obstacles)
				strbuilder.append("("+ob+")");
		}
		return strbuilder.toString();
	}
}
