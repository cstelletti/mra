package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MarsRover {
	
	int planetX;
	int planetY;
	boolean[][] planet;
	Rover rover;
	String direction;
	final String[] directions = {"N","E","S","W"};
	
	Pattern p = Pattern.compile("(^\\((\\d+)\\,(\\d+)\\)$)");
	Matcher m;
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		if(planetX<0 || planetY<0)
			throw new MarsRoverException("Attention! The dimension of the planet cannot be negative");
		else {
			this.planetX = planetX; 
			this.planetY = planetY;
			this.planet = new boolean[planetX][planetY];
			this.rover = new Rover(0,0,"N");
		}
		for(String obs : planetObstacles) {
			m = p.matcher(obs); 
			if(!m.matches() || Integer.parseInt(m.group(2))>=planetX || Integer.parseInt(m.group(3))>=planetY) {
				throw new MarsRoverException("Attention! There is an obstacle that dont't have the right coordinates of the planet! Otherwise it is written in the wrong way (It will not included in the obstacles)");
			}
			else {
				this.planet[Integer.parseInt(m.group(2))][Integer.parseInt(m.group(3))] = true;
			}
		}
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) {
		return planet[x][y];
		
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		int i;
		List<String> obstacles = new ArrayList<>();
		for(i=0;i<4;i++) {
			if(rover.getDir().equals(directions[i]))
				break;
		}
		if(commandString=="")
			return rover.getStatus(null);
		else{
			for(char car : commandString.toCharArray()) {
				if(car == 'r') {
					i=(i+1)%4;
					rover.setDir(directions[i]);
				}
				else if(car == 'l') {
					i=(i+4-1)%4;
					rover.setDir(directions[i]);
				}
				else if ((car == 'f' && rover.getDir().equals("N") || car == 'b' && rover.getDir().equals("S"))) {
					if(!planetContainsObstacleAt(rover.getX(), (rover.getY()+1)%planetY))
						rover.setY((rover.getY()+1)%planetY);
					else if(!obstacles.contains(rover.getX()+","+(rover.getY()+1)%planetY))
						obstacles.add(rover.getX()+","+(rover.getY()+1)%planetY);
				}
				else if((car == 'f' && rover.getDir().equals("E") || car == 'b' && rover.getDir().equals("W"))) {
					if(!planetContainsObstacleAt((rover.getX()+1)%planetX, rover.getY()))
						rover.setX((rover.getX()+1)%planetX);
					else if(!obstacles.contains((rover.getX()+1)%planetX+","+rover.getY())) 
						obstacles.add((rover.getX()+1)%planetX+","+rover.getY());
				}
				else if((car == 'f' && rover.getDir().equals("S") || car == 'b' && rover.getDir().equals("N"))) {
					if(!planetContainsObstacleAt(rover.getX(), (rover.getY()+planetY-1)%planetY))
						rover.setY((rover.getY()+planetY-1)%planetY);
					else if (!obstacles.contains(rover.getX()+","+(rover.getY()+planetY-1)%planetY)) 
						obstacles.add(rover.getX()+","+(rover.getY()+planetY-1)%planetY);
				}
				else if((car == 'f' && rover.getDir().equals("W") || car == 'b' && rover.getDir().equals("E"))) { 
					if(!planetContainsObstacleAt((rover.getX()+planetX-1)%planetX, rover.getY()))
						rover.setX((rover.getX()+planetX-1)%planetX);
					else if(!obstacles.contains((rover.getX()+planetX-1)%planetX+","+rover.getY()))
						obstacles.add((rover.getX()+planetX-1)%planetX+","+rover.getY());
				}
				else
					throw new MarsRoverException("This command cannot be considered (Remember you can insert only l,r,f,b)");
			}
		}
			
		return rover.getStatus(obstacles);		
	}

}
