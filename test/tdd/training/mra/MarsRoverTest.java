package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {
	
	//Test User Story 1
	@Test(expected = MarsRoverException.class)
	public void testMarsRover() throws MarsRoverException {
		//This method tests the coherence among the input values (in this case there is
		//an exception because the second obstacle dont'match the pattern)
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,N)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertTrue(rover!=null);
	}
	
	@Test(expected = MarsRoverException.class)
	public void testMarsRover1() throws MarsRoverException {
		//This method tests the consistency among the input values (in this case there is
		//an exception because the x-coordinate of the first obstacle is not consistent with the x dimension of the planet )
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(10,5)");
		planetObstacles.add("(7,2)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertTrue(rover!=null);
	}
	
	@Test
	public void testPlanetContainsObjectAt() throws MarsRoverException {
		//This method tests the presence of an obstacle in the planet (at x, y coordinates).
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		boolean obstacle = rover.planetContainsObstacleAt(4, 7);
		assertTrue(obstacle);
	}
	
	//Test User story 2
	@Test
	public void testExecuteCommand() throws MarsRoverException {
		//This method tests the executeCommand() method with an empty string
		String s = "";
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,6)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		String returnstring = rover.executeCommand(s);
		assertEquals(returnstring, "(0,0,N)");
	}
	
	
	//Test User story 3
	@Test
	public void testExecuteCommand2() throws MarsRoverException {
		//This method tests the executeCommand() method with "r" string
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,9)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		String commandstring = "r";
		String returnstring = rover.executeCommand(commandstring);
		assertEquals(returnstring, "(0,0,E)");
	}
	
	@Test
	public void testExecuteCommand3() throws MarsRoverException {
		//This method tests the executeCommand() method with "l" string
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,6)");
		planetObstacles.add("(7,2)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		String commandstring = "l";
		String returnstring = rover.executeCommand(commandstring);
		assertEquals(returnstring, "(0,0,W)");
	}
	
	@Test(expected=MarsRoverException.class)
	public void testExecuteCommand4() throws MarsRoverException {
		//This method tests the executeCommand() method with "a" string (that is not allowed)
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,1)");
		planetObstacles.add("(6,8)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		String commandstring = "a";
		String returnstring = rover.executeCommand(commandstring);
	}
	
	//Test User story 4
	@Test
	public void testExecuteCommand5() throws MarsRoverException {
		//This method tests the executeCommand() method with "f" string
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,1)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		rover.rover.setX(7);
		rover.rover.setY(6);
		String commandstring = "f";
		String returnstring = rover.executeCommand(commandstring);
		assertEquals(returnstring, "(7,7,N)");
	}
	
	//Test User story 5
	@Test
	public void testExecuteCommand7() throws MarsRoverException {
		//This method tests the executeCommand() method with "b"
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,4)");
		planetObstacles.add("(3,3)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		rover.rover.setX(5);
		rover.rover.setY(8);
		rover.rover.setDir("E");
		String commandstring = "b";
		String returnstring = rover.executeCommand(commandstring);
		assertEquals(returnstring, "(4,8,E)");
	}
	
	//Test User story 6
	@Test
	public void testExecuteCommand8() throws MarsRoverException{
		//The command string can contain a combination of single commands (i.e., �r�, �l�, �b�, and �f�).
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		String commandstring = "ffrff";
		String returnstring = rover.executeCommand(commandstring);
		assertEquals(returnstring, "(2,2,E)");
	}
	
	//Test User story 7
	@Test
	public void testExecuteCommand9() throws MarsRoverException{
		//When the rover goes beyond an edge, its new position is at the opposite edge. 
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		String commandstring = "b";
		String returnstring = rover.executeCommand(commandstring);
		assertEquals(returnstring, "(0,9,N)");
	}
	
	//Test User story 8
	@Test
	public void testExecuteCommand10() throws MarsRoverException{
		//The rover can encounter an obstacle while executing commands and thus moving on the planet. 
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		String commandstring = "ffrfff";
		String returnstring = rover.executeCommand(commandstring);
		assertEquals(returnstring, "(1,2,E)(2,2)");
	}
	
	//Test User story 9
	@Test
	public void testExecuteCommand11() throws MarsRoverException{
		//The rover can encounter multiple obstacles while executing a command and thus moving on the planet
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		String commandstring = "ffrfffrflf";
		String returnstring = rover.executeCommand(commandstring);
		assertEquals(returnstring, "(1,1,E)(2,2)(2,1)");
	}
	
	//Test User story 10
		@Test
		public void testExecuteCommand12() throws MarsRoverException{
			//(The rover can encounter an obstacle when trying to go beyond an edge)
			List<String> planetObstacles = new ArrayList<>();
			planetObstacles.add("(0,9)");
			MarsRover rover = new MarsRover(10, 10, planetObstacles);
			String commandstring = "b";
			String returnstring = rover.executeCommand(commandstring);
			assertEquals(returnstring, "(0,0,N)(0,9)");
		}
	
	//Test User story 11
	@Test
	public void testExecuteCommand13() throws MarsRoverException{
		//(The rover takes a tour around the planet, encountering several obstacles and going beyond edges in any dimension)
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(0,5)");
		planetObstacles.add("(5,0)");
		MarsRover rover = new MarsRover(6, 6, planetObstacles);
		String commandstring = "ffrfffrbbblllfrfrbbl";
		String returnstring = rover.executeCommand(commandstring);
		assertEquals(returnstring, "(0,0,N)(2,2)(0,5)(5,0)");
	}
}
